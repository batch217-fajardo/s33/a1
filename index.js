// console.log("Hello World");

// 3 & 4
async function fetchData(){
		let result = await fetch("https://jsonplaceholder.typicode.com/todos");
		let json = await result.json();
		console.log(json)


		let titles = json.map((title) => ({
			title: title.title
	}));
		console.log(titles);
	
	}
	fetchData();


	fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(`The item "${json.title} on the list has a status of ${json.completed}.`, json))

fetch("https://jsonplaceholder.typicode.com/todos", {
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			userId: 1,
			id: 201,
			title: "Created To Do List Item",
			completed: false
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));




	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			id: 1,
			title: "Updated to do list item",
			description: "To update the my to do list with a different data structure",
			userId: 1,
			status: "Pending",
			dateCompleted: "Pending"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));



	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "07/09/21"			
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	});

